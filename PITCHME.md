# cctas
### Deploying services


+++

# Introduction <!-- .element class="top-center" -->

Three basic models for Cloud Computing

- **I**nfrastructure **a**s **a** **S**ervice (_**IaaS**_)
  <!-- .element class="fragment" -->

- **S**oftware **a**s **a** **S**ervice (_**SaaS**_)
  <!-- .element class="fragment" -->

- **P**latform **a**s **a** **S**ervice (_**PaaS**_)
  <!-- .element class="fragment" -->


 Many more _XXXaaS_, targetting specific vertical areas

<!-- .element class="fragment" -->

note: 

Many different XXXaaS proposals, targeted to specific application areas. What they really are
are frameworks focused to develop specifica kinds of applications.


+++
## Classic CC Layer image

<!---![](https://snappygoat.com/o/d18537ec6752d4781913a099c27db96bdb5378f7/Cloud%20computing%20layers.png) -->
![](assets/images/Cloud_computing_layers.png)


+++

## Infrastructure as a Service
#### Managing Virtualized physical assets

* Machines
* Networks

You put everything else:

The software stack

<!-- .element class="fragment" -->


---?include=topics/virtualization/basics.md

<!-- ---?include=containers/PITCHME.md

---?include=docker/PITCHME.md -->

---?include=topics/async/basics.md

---?include=topics/async/javascript.md

---?include=topics/XaaSProject/0_basics.md

---?include=topics/XaaSProject/1_basic_faas.md

---?include=topics/XaaSProject/2_basic_scale_out.md

---?include=topics/XaaSProject/3_asynchrony.md