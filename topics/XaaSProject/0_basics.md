# Cloud Paradigms

Utility-like computing

---

## Cloud paradigm in a nutshell

Use only what you need when you need it
<!-- .element class="fragment" -->

Pay only for what you use
<!-- .element class="fragment" -->

**_Leave operations to somebody else_**
<!-- .element class="fragment" -->

+++

## How to achieve cloud benefits?
- Efficiently apportion and assign resources to run software
<!-- .element class="fragment" -->
  - **Gain**: 
    - Resources fully utilized 
      - Among all agents (**_tenants_**)
  - **Pain**: 
    - Needs change in time
      - Dynamically modify each tenant's resources.

+++

## Answer (partial) IaaS
- Virtualization of hardware
- Virtualization of network
- Accounting of usage
  - Along various time periods

+++

## How to achieve cloud benefits?
- SaaS needs more than creating great code
  - Software needs to be properly deployed
    - To adequately handle requests
  - Adequacy == Fulfillment of some QoS
    - Need to scale to avoid violating QoS

<div>
IaaS fall short to help on this issue
  - Need more automation
<!-- .element class="fragment" -->
</div>
<!-- .element class="fragment" -->

---

# PaaS

+++

## What can you do ... 

## ... without an OS
<!-- .element class="fragment" -->

# Everything!!!!
<!-- .element class="fragment" -->

## at a cost
<!-- .element class="fragment" -->

+++

## Running directly on top of an IaaS is like

...
Working without an OS ...
<!-- .element class="fragment" -->

You can do everything... 
<!-- .element class="fragment" -->

# but at what cost!!
<!-- .element class="fragment" -->

+++

## Things you need to worry about

- Choose how to best componentize your service app
  - Each part may have different needs for resources
  - Each part may have different criticallity requirements
  - Each part may have different scaling characteristics

+++
### ... Things you need to worry about

- Some parts may already be implemented
  - With their own unalterable characteristics
    - Legacy
- Some parts may already be offerend as a service elsewhere
  - With their own protocols

+++

### ... Things you need to worry about

For each part/component
- What is the best OS to run it under
  - Libraries?
- How does it find the other parts it depends on?

- How does it find other services it needs?
  - How does it get the credentials it needs for access?

+++

### ... Things you need to worry about
... For each part/component

- How does it expose the functionality to other parts/services/clients
- How does it cope with increased/decreased load
  - How many resources does it need for 1 user?
    - For n users? ...
- How does it adapt to failures?
- How does it find new instances of other components when they are recovered/duplicated (for scale)

+++
# ... many, many More ...

+++

# What do you do with all those worries 
without an OS?

+++

# On an IaaS
## What can help?

a PaaS
<!-- .element class="fragment" -->

+++

## The Shoulds of a PaaS
#### Helps structuring your service application
- Which components are needed
- How components are related
- How scaling affects one another

+++

## ...The Shoulds of a PaaS
#### Helps maintain the service up

- Recovers Failures 
  - Supports continuity through wise replica placement
- Provides mechanisms for automatic deployment
  - Selects appropriate VMs
- Provides automatic scaling
  - Elasticity on scalable services

+++

## ...The Shoulds of a PaaS
#### ... Helps maintain the service up

- Auto-configures on changes
  - Fault recoveries
  - Scaling decissions
  - Software changes


---

# PaaS Concepts

+++

## PaaS is the OS of the Cloud
Well... it ought to be
<!-- .element class="fragment" -->

- Otherwise, similar concepts to an IaaS
  - Instances of microservices
    - Each one represents a long-lived computation
    - Self-contained functionality
- Composition of scalable microservices...
  - ...form a service
<!-- .element class="fragment" -->

+++

### Basics: Isolation
#### Do we need it?

Issues:

- Can a computation from a service observe data from another?
  - Stored
  - Transmitted
- Can a computation from a service steal resources from another?
  - Can steal CPU?, Memory? Bandwidth?
- Can a computation from a service inject data/code in another?
  - Exploit vulnerabilities...

+++

### Basics: Isolation
#### We need it!

- How to isolate instances of microservices?
  - VMs?
  - ??? <span>Containers</span>
<!-- .element class="fragment" -->
    - How about vulnerabilities?
<!-- .element class="fragment" -->
- How do we isolate communications between containers?
  - Encrypted protocols?
  - ??? <span>Full isolation with controlled Paths</span>
<!-- .element class="fragment" -->
    - Impossible to subvert what you cannot reach
<!-- .element class="fragment" -->

NOTE: 

Containers are, in principle, more vulnerable than VMs?
However, they are more manageable and allow quiquer times and
better usage of underlying resources

---

### Basics: Deployment
#### Issues: deployment

- What size VM do we need to allocate
  - VMs are the units provided by IaaS
- What size network do we need?
  - How many networks do we need?
- Which VMs should be used for the instances of microservices
  - Issues
    - Avoid fragmentation
    - Avoid critical failures
    - Optimize cost from IaaS

+++

### Basics: Deployment
#### Issues: configuration

- How do instances of each microservice find instances of other microservices?
- How do global service configuration reaches each microservice instance
  - How is it converted?
- How are instances warned of dynamic changes of configuration
  - How are they told about relevant changes in instances of microservices?


NOTE:

---

### Basics: Dynamics
#### Changing circumstances and lifecycle management

- Software changes
  - Upgrades
  - Fixes
- Load changes
  - More/less requests on services
  - More/less data to be stored

---

## PaaS over IaaS
- Outsources management
  
  > PaaS frees developers from taking care of deployment issues
<!-- .element class="fragment" -->

- Goal: 0-devops computing
<!-- .element class="fragment" -->

> But still counting instances of servers
<!-- .element class="fragment" -->

NOTE:
Servers are long term entities

---

### Can we improve on PaaS?

- A server uses resources constantly...
  - Pays for them, irrespective of usage
  - The minimal elasticity is the instance
    - But instances must be designed as long lived services
- Can we pay only for the time of activity?
  - Difficulty
    - Accounting for CPU actions
      - Accounting for time waiting
  
+++

# FaaS

aka Serverless Computing
<!-- .element class="fragment" -->

+++

## Questions

- How to pay for activity launched?
- How to define what can be activated?
- How to deploy the code?
- How to keep latency in check?

+++

### FaaS can be created as a Service on a PaaS 

- PaaS takes care of scaling up/down
- FaaS owner pays PaaS as per above
- FaaS owner needs to figure out how to charge its customers

+++

### FaaS users need to register... what?
>Function as a Service
<!-- .element class="fragment" -->

That should give a hint...

> Functions
<!-- .element class="fragment" -->

---

## Basic operation

- User registers a function
- Later on, user calls the function
  - Parameters, sufficient to carry out the operation
    - Includes credentials to access other services
    - Besides standard data-like Parameters
    - Potentially references to other Functions

+++

## Problems to solve 

- Activate the function JIT
  - Low latency
- Provide specific API?
- Make sure function runs within some resource bounds
  - How, when to specify them?
- Make sure function does not interfere with something else 

+++

### Other problems?


---

# Project
## Faas

on a PaaS

+++

## Goals

- Study PaaS (https://discover.kumori.cloud)
- Design approach
- Determine shortcomings of PaaS
  - Propose functionality additions
- Reason about latency
- Describe additional API support (if any)

+++

## Results

- Design paper 
- API 
- Code on KPaaS

+++

## approach

- Study proposals on nodejs
- Use docker-compose to test initial approaches
- Use local stamp for KPaaS
- Deploy on actual PaaS 
  
