# Asynchrony

---

## Decouple function execution request from response

- In our current approach, clients wait till FaaS finishes executing
- Plenty of scenarios can benefit from avoiding this waiting period

+++

### Things to worry about

- Executions already store their results in a Broker's queue
  - In SYNC execution, this queue's size is bounded
  - In ASYNC...
    - We need to store results until they are delivered to the client
      - Scalability issues (probably)

+++

### More worries

How to let clients retrieve their results?

- Generate a call ID on invocation
- Let client use another REST call to retrieve:
  - GET `https://domain/RetrieveResult/<call ID>`
  - Returns a JSON string

---

## Addressing the scale issue

- Output queue size is not coupled with input queue size
  - It depends on external client actions
- It works as a store on behalf of clients
  - Read-only
    - Individual queue items are immutable
- How to scale it?

+++

### Install it on a different component

- Out of the Broker
- Depending on how results are delivered to clients
  - Broker needs to receive notifications
  - or
  - Broker simply requests items from output queue