# From one spawner to many

- Our scheme will run into a wall when resources are exhausted on the host
  - GOAL: expand our basic FaaS to a scaling architecture

How?

---

## Broker -- Worker architecture
### The Broker

- Front End acts as a Broker
  - Builds a queue of waiting jobs
    - Available workers ask for jobs to complete
    - You have workers for building images
      - And workers for running functions

+++
### The worker

- Special image 
- Must cooperate with Broker
  - How? Propose interaction patterns
    - E.g., ask for next job in the broker's queue
    - Push back the result of a job

---

## What to do with the images

- All workers must be able to access all images (i.e. functions)
  - Set up a docker registry
    - Who pushes images to it?
    - A worker launching a job needs to access it.

# Your job

* Execute on this basic scale-out scheme
* Mount everything as a docker-compose file
    * Broker - 1
    * registry - 1
    * Worker - n
* Test multiple configurations involving various numbers of workers
* Devise a stress test

