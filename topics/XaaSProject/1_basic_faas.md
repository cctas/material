# A Basic FaaS design

API driven

---

## High Level

### Two main API calls

* `RegisterFunction`
* `InvokeFunction`

+++

#### To determine:

* Transport
* Signature
* Function representation
    - Restrictions
  
---

## Transport 
Several possibilities

* Plain tcp
  - Cumbersome: byte stream to be coerced to a structured messaging
* HTTP/REST
  - Highly available
  - Plenty of tools
  - Downside: text-based, potentially inefficient

+++

## Transport

* ZMQ
  - Highly flexible
  - Efficient: native binary
  - As structured as needed
* ...

Let us choose, for simplicity, HTTP
---

## Function representation

Very many choices

* Language/runtime choice: uncountable
  - For simplicity let us choose nodejs
* Simple function in a file
* Multi file payload
    * What about dependencies?
        * Make it a full-fledged npm module

* In both cases need to worry about dependencies
    * Let `package.json` specify them

---

## Function representation
### Restrictions

Assume npm module (simple, well understood)

* How to securely bring in dependencies
    * Restrict size?
    * Restrict binary compilations?
    * Restrict bandwidth needed to build... what?
    * Restrict versions of available nodejs engine?
    * Restrict coms capabilities?
    * ...

+++

# For now, we restrict almost nothing

---

## Signature
### `RegisterFunction`

* Input: npm module as tgz 
* Output: sha256 digest of the given tgz (_fid_)
* REST
    * URL: https://domain/RegisterFunction
    * Method: POST
    * Body: tgz file as multipart content
    * Return: Body with sha256 string

---

## Signature
### `InvokeFunction`

* Input: a JSON body
* Output: A JSON body, synchronously provided
* REST
    * URL https://domain/InvokeFunction/fid (sha256)
    * Method: PUT
    * Body: JSON input
    * return: Body with JSON output

---

# Operations

To determine

* What do we do with the Registration?
* What are the mechanics of the invocation?

Closely related questions

---

## Mechanics of invocation

Main concern is security!

Are we sure we want to run arbitrary code?

if so, how do we protect ourselves?

---

## Use docker containers

Basic approach 

* On invocation, run the package within an appropriate container
    * NOTE: The bae image will determine restrictions for the software in the npm module.
    * How to we ready the module?
  
  Need to perform an npm install first

  Alternatives?

+++

## Keep a start script in the base image

* Run the script as the cmd of the `docker run`

Where does it find the code?

* In a mapped volume from the host launching
    * Need to organize functions in the host file system
  
  Problem: Latency

---

### How about building an image first?

* On function Registration
    * Also from a base image
    * We can keep the idea of the start script, if useful
    * We can have a generic Dockerfile to help build
* Question: Would it help registering the image in a registry?
    * Which one?


+++

### Invoke by launching the image 

* Passing the JSON body
    * Build a host folder
        * Place the JSON in a file: data.json
* Handling the JSON result
    * Use a file: result.json within above folder

---

# Your job

Execute on this simple approach 

Try to embed the FaaS within a docker container

Measure times with simple examples

make project `basic_faas` in gitlab





