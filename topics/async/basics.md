# Async Programming

Wait-free concurrency...

---

## Wait questions

- Who waits?
  - Threads
<!-- .element class="fragment" -->
- Why waiting?
  - Block until a request completes
<!-- .element class="fragment" -->
  - Block until a critical section is unlocked
<!-- .element class="fragment" -->
- How to get concurrency when Sync programming
  - Multi-threading
<!-- .element class="fragment" -->
  - Concurrency control primitives
<!-- .element class="fragment" -->

---

## Do not wait questions

- How to get concurrency
  - No wait... multiple things happening!
<!-- .element class="fragment" -->
- How to avoid inconsistency
  - No preempitve scheduling
<!-- .element class="fragment" -->
  - Release CPU when convenient
<!-- .element class="fragment" -->
- How many async threads are needed
  - 1
<!-- .element class="fragment" -->
  - More...
<!-- .element class="fragment" -->
    - dangerous
<!-- .element class="fragment" -->
    - Read-only structures
<!-- .element class="fragment" -->

NOTE:
- Functional programming languages make this easy
  - Explicit modeling of side effects
  - No mutable memory
