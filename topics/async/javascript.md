<style>
.reveal pre code {
    max-height: 600px
}
</style>

# Javascript
Non-pure functional

Async programming

Prototypical object oriented

---

## Compiled?

- Not really
- Interpreted
- Portable
  - Different environments
  - Different runtimes
- Standardized (ECMASCRIPT or ES)

---

## Characteristics

### Object oriented
- Prototypical inheritance
- Classes can be built on top
  - ECMASCRIPT 6 introduces **_class_** construct

+++

### functions are objects
  - dynamically built
    - can be anonymous
  - Support _closures_

+++

### No threading Support

- All runtimes must support async mechanisms
    - with _callbacks_
- Native promises in ES 6
    - Particular case of Functional Reactive Programming
    - **_async/await_** constructs in ES6 supporting sync-like functions

---

## Main runtimes

- Browser
  - Part of HTML5 standard
    - DOM manipulation

- Server
  - nodejs most popular
  - npm: package support system
    - Module distribution

---

## Installation
- Browser: already there
  - With debugging support
- Server: install [nodejs](http://www.nodejs.org)
- Through editors
  - Like VS Code

---

## NodeJS

- Runtime derived from Chromium V8
- Efficient, including interfaces to major OS APIs
  - Heavily unix-oriented
  - Async-first
    - Some Synchronous calls
    - callback-based
      - signature: 
```js
  function cb(err, data) {...}
```
- Uni-threaded

---
## Inheritance & Closures

+++?code=assets/js/closures/0_scope.js&lang=javascript&title= Scopes

![A function scope](assets/images/closures/0_scope.png )
<!-- .element style="height:60%; width:60%" -->

+++?code=assets/js/closures/1_prototype.js&lang=js&title=Prototypical inheritance

![prototypes](assets/images/closures/1_prototype.png )
<!-- .element style="height:30%; width:30%" -->

+++?code=assets/js/closures/2_global.js&lang=js&title= The global object

![prototypes](assets/images/closures/2_global.png )
<!-- .element style="height:60%; width:60%" -->

+++?code=assets/js/closures/3_two_closures.js&lang=js&title= Dynamic closures

![prototypes](assets/images/closures/3_two_closures.png )
<!-- .element style="height:60%; width:60%" -->

+++?code=assets/js/closures/4_mix.js&lang=js&title= Combined example

+++

### Combined example

![prototypes](assets/images/closures/4_mix.png )

---

# Code examples

<div>
+++?code=assets/js/javascript/j00.js
<!-- .element style="font-size:30%" -->
</div>

+++?code=assets/js/javascript/j01.js
<!-- .element style="font-size:30%" -->

+++?code=assets/js/javascript/j02.js
<!-- .element style="font-size:30%" -->

+++?code=assets/js/javascript/j03.js
<!-- .element style="font-size:30%" -->

+++?code=assets/js/javascript/j04.js
<!-- .element style="font-size:30%" -->

+++?code=assets/js/javascript/j05.js
<!-- .element style="font-size:30%" -->

+++?code=assets/js/javascript/j06.js
<!-- .element style="font-size:30%" -->

+++?code=assets/js/javascript/j07.js
<!-- .element style="font-size:30%" -->

+++?code=assets/js/javascript/j08.js
<!-- .element style="font-size:30%;" -->

+++?code=assets/js/javascript/j09.js
<!-- .element style="font-size:30%" -->

---

# Asynchrony in nodejs

+++
<!--  .slide: data-transition="none" -->

![image](assets/images/async/a0.png)

+++
<!--  .slide: data-transition="none" -->

![image](assets/images/async/a1.png)

+++
<!--  .slide: data-transition="none" -->

![image](assets/images/async/a2.png)

+++
<!--  .slide: data-transition="none" -->

![image](assets/images/async/a3.png)

+++
<!--  .slide: data-transition="none" -->

![image](assets/images/async/a4.png)

+++
<!--  .slide: data-transition="none" -->

![image](assets/images/async/a5.png)

+++
<!--  .slide: data-transition="none" -->

![image](assets/images/async/a6.png)