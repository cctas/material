# Virtualization

---

## Virtualization: the basics

So, what is it?   

> Mechanism to substitute the environment for which a software was designed to run...
<!-- .element class="fragment" -->

> ... for another, emulated on top of a different environment
<!-- .element class="fragment" -->

---

## Machine Virtualization

- The hardware is "emulated" by a piece of software
  - The hypervisor
- The hypervisor is in charge of the execution of a complete OS
  - Kernel included
      - Requires emulating devices compatible with the OS kernel

+++

## Machine Virtualization
#### Hypervisors
 
- VirtualBox
- VMWare
- Hyperv
- Xen
- ...
  - Many more different technologies
  - Mature field

note:

- The set of hypervisor technology is abundant, as early on, 
its value was recognized to allow execution of software created for different
platforms to execute on a platform different from that intended for them.

- Clear delineation of what it is to be emulated. 
  - further bolstered by predominance of particular hardware platform (Intel)
- Well understood surface of attack

---

## Network Virtualization

- The network (communication layer) is emulated at various layers: L2 and L3

- Originally born out of various needs
  - _soft_ switches for Virtual machines
  - _VPN_ 's to address the needs of connecting to private LANs

+++

### L2 virtualizarion

- Builds an "ethernet" layer 
  - On top of, typically, an L3/L4 layer.
  - Broadcast domains
- Nodes can act on ethernet interfaces (virtual)
  - These interfaces are manipulated by the software stack as if they where real.

+++

<img src="assets/images/netvirt.png" width="500px">

+++

### L3 virtualization
- Builds an "IP" network on top of, typically, an L3/L4 layer
  - No broadcast domains
  - Also, apparent ethernet interfaces available (virtual)
    - However, limited management of devices
      - Only at the L3 level

+++

# SDN 
## Software Defined Networks

+++

### Software Defined Networks

- zerotier
  - L2
- wireguard
  - L3 (with routing)
- ...
  - More technologies
  - Not as mature as Hypervisor technology
  - Proprietary in some cases

note:

- Scarcity of offers speaks to the relatively novelty of the approaches (compared to hypervisors)
- Original motivation: VPNs
- Gone well beyond that in approaches, but still covering original concerns.
- The fact that some solutions are proprietary indicates the low level of maturity...

---

## OS-level (_light_) virtualization

- Only the lowest layers of an OS are virtualized

#### Kernel <!-- .element class="fragment" -->

- It takes care of executing the higher layers
  - Provides isolated environment 
    - For its processes <!-- .element class="fragment" --> 

note:

more on this later: containers, and all that good stuff

---

# Why Virtualization?

- Isolate activity <!-- .element class="fragment" --> 
- Ease management <!-- .element class="fragment" --> 

---

## Isolation

#### What is it? 
<div>

- Execution environment 
- Keeps processes within from
  - Affecting other processes outside
  - Being affected by other processes from outside
</div><!-- .element class="fragment" --> 

note:

Mechanisms already exist in OS to obtain isolation: Processes

However, design of such mechanisms, and their interactions with the rest 
of the OS has produced a less than ideal isolation
mechanism

API surface area too wide 

+++

## Requirements of Isolation

- Strict partitioning of resource pools
- Strict restriction to share resources
- Strict restriction of communications

+++

## Partition resource pools

- A big machine is split flexibly
  - Adapts, potentially, to circumstances
- Particions along various resource dimensions
  - CPU (usage, cores)
  - RAM
  - Local Disk
  - IOPS
  - Bandwidth
  - ...


note:

1. E.g., a hoster can divide up a machien in the sweet spot among its customers
  - Savings for the hoster
  - Potential savings for the customer


+++

## Share-nothing for resources (by default)

- No default mechanism to allow access to shared resources
  - Each isolated environment has its own set of usable resources
    -  TBD how it is initialized
    -  TBD how it is accessed
    -  TBD how are *resources* finally obtained
  - Related to the concept of a _Capability_

note:
- This is tremendously important in legacy situations

+++

## Aside
#### What is a capability?

>A capability is a "handle" on an object that allows whoever has it to run 
methods on that object, according to the capability interfaces
<!-- .element class="fragment" --> 
 
+++

## Competing Mechanisms


### Naming resources 
vs 
### Dependency injection

+++

## Naming Resources

- Classic, well understood approach
- A process needs to "open" (resolve) a globally valid name
  - Goal: obtain a "capability" to a resource, e.g. 
    - File System
    - Network entities (DNS, IP addresses)
  - Typically on a shared namespace


+++

## Naming Resources

#### Isolation requirements

- Construct independent namespaces
  - On an OS
    - Files, Processes, devices, network interfaces, ...
  - On the network
    - DNS names, network domains (IP)

+++

## Dependency Injection

- Resources are sort of dependencies
  - Need them to carry out a job
- For proper reuse resource providers
  - Those are injected <!-- .element class="fragment" --> 
- Each provider responsible for its own methods <!-- .element class="fragment" --> 
  - Avoid global naming schemes
  - Own injection approach

### Requires design-time<!-- .element class="fragment" --> 

+++

## Communications restriction

- A particular case of share-nothing
- Specific comm paths established for dependencies
- Dependency injection of paths
  - A path is like a capability to a "remote" object
  - Can be mostly hid from client processes

+++

## Communications restriction
### Consequences for isolation:
- No direct URL spec
  - Not in the code (of course!!!!)
  - Not even as configuration
    - It tends to linger around
  - Not as an algorithm
    - Too many assumptions about the underlying mechanism

+++

## Communications restriction
### Consequences for management

- Keep it high level
- Is it an API call?
  - Use native mechanisms of the programming environment
  - Never pollute it with low level details
    - e.g Access Rights
    - Protocol (ports?)
  - E.g. make it akin to a _Capability_

+++

## Communications restriction
### inject/resolve

- Inject the driver
  - Explicit design decission
- Resolve
  - Use locally significan names 
  - No global namespace
    - Names cannot be passed around for others to resolve
    - Avoid creating a shared reosurce when none is intended

---

## Management

- Artifacts as objects: fully manageable on their own
  - No surprises
  - Storable/trackable representation
- E.g. Networks as Artifacts
  - population under full control
  - paths under full control
  - Rules can be stored/tracked/dynamically modified

+++

## Management
### Virtualized images 
  - Stack of technologies needed for execution
  - All dependencies included (++--)
  - Akin to statically linking
    - Multiple apps for multiple processes
      - Depends on runtime
  - Easy to deploy
    - With proper metadata, automatic configuration

note:
  With metadata, the runtime for execution

+++

## Management
### Host Agents
#### e.g. for VMs

- listens for insructions from central control
  - Can create new VMs
  - Can obtain images for those VMs
  - Can configure the VM partition 
    - CPU, RAM, ...
  - Can setup virtualized resources (networks, disks)
  - Can boot the VM


+++

## Management
### Host Agents
#### e.g. for light virtualization

- Can create some sort of container
- Can setup a stack of software 
- ...
- ... can start a process within the container

---

## Machine vs Light virtualization

+++
### VMs

- Need to boot a complete OS (the guest OS)
  - Boot process identical to that performed by a physical machine
    - Takes time
- In general, needs to start many processes to provide the machine environment
  - Takes more resources to simulate the machine environment

+++
### VMs

- Wastes resources
- Whole spectrum of resources must be available for the booted os
- Limited resource sharing options among VMs
  - Code cannot be shared among different guests

+++
### VMs

  - Host OS can be completely different from guest OS
    - E.g. Windows/Linux
    - Windows/MacOSX
    - MacOSX/Linux
    - MacOSX/Windows
    - Linux/*
  - Small attack surface (Hardware layer)

+++

<img src="assets/images/VMvirtualization.png" width="500px">

+++

### light virtualization

- Container-like entities (light virtualization)
  - Simply needs to set up the isolation environment
    - Needs support from the OS kernel
    - Namespace isolation
      - Thus communications primitives isolation
        - Including IPC mechanisms
        - Including virtualized network interfaces
    - User space isolation
      - Superuser (root in Linux) inside has nothing to do with root at the host
      - Can be considered a special case of namespace isolation


+++
### Light virtualization

- Resource capping
  - CPU/memory/etc... 
- Code can be shared among containers
- Constrain: guest must work on Host’s kernel API...


+++

### Light virtualization

<img src="assets/images/lightvirt.png" width="500px">

+++

#### Mixed Approaches

Containers within VMs

- Docker-machine 
- docker for Windows
- docker for Mac
- Docker on any IaaS (AWS, Azure, ...)

+++

<img src="assets/images/mixedvirt.png" width="400px">